
><img src="Logo_TextStyle_Editions.jpg" width="200" height="75" alt="TEXTSTYLE EDITIONS">


# Quelques intructions pour la mise en oeuvre des bonnes pratiques Git

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

Démarche à suivre pour pousser votre code LaTeX sur le dépôt distant.

Cette procédure prend en compte les codes contenant des fichiers PDF de plus de 100 Mo. Suivez la procédure dans son intégralité afin d'éviter tout désagrément relatif à des fichiers volumineux.

Pour utiliser Git LFS pour gérer les fichiers PDF dans votre dépôt sans les supprimer, suivez ces étapes :

### Étapes pour configurer Git LFS pour les fichiers PDF

1. **Initialiser Git LFS** :
```bash
git lfs install
```

2. **Configurer Git LFS pour suivre les fichiers PDF** :
```bash
git lfs track "*.pdf"
```

3. **Ajouter le fichier `.gitattributes` généré** :
```bash
git add .gitattributes
```

4. **Ajouter et committer tous les fichiers PDF** :
```bash
git add .
```
```bash
git commit -m "Ajout des fichier pdf suivi par LFS"
```

5. **Migrer les fichiers PDF existants vers Git LFS** :
```bash
git lfs migrate import --include="*.pdf"
```

6. **Pousser les changements au dépôt distant** :
```bash
git push --set-upstream origin main
```

```bash
git push --force --set-upstream origin main
```

7. **Vérifier les fichiers suivis par Git LFS** :
```bash
git lfs ls-files
```

### Usage du ficher bash script push_code.sh

Vous pouvez exécuté le fichier bash afin de pousser votre code latex sur le dépot distant, ce qui est pratique et vous éviterra d'entrer à la main la succession de commandes évoquées précédemment.

1. Rendre exécutable le script
```sh
chmod +x push_code.sh
```

2. Exécuter le script pour pousser votre commit LaTeX
```sh
./push_code.sh
```

Force à vous pour la suite 😀🥳

Carlinfg.